package com.codinglabs.rmi.pub.dto;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Setter
@Getter
@ToString
public class PersonalInfo implements Serializable {

	private int id;
	private String firstName;
	private String middleName;
	private String lastName;

}