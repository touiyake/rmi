package com.codinglabs.rmi.pub.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

import com.codinglabs.rmi.pub.dto.PersonalInfo;

public interface UserInfo extends Remote {

	void addInfo(PersonalInfo info) throws RemoteException;
	
	PersonalInfo getInfo(int id) throws RemoteException;
	
	Map<Integer, PersonalInfo> getAllInfo() throws RemoteException;
	
}