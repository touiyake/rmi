package com.codinglabs.rmi.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import com.codinglabs.rmi.client.ssl.SSLFile;
import com.codinglabs.rmi.client.ssl.SslClientSocketFactory;
import com.codinglabs.rmi.client.util.Util;
import com.codinglabs.rmi.pub.dto.PersonalInfo;
import com.codinglabs.rmi.pub.interfaces.UserInfo;

public class RmiClientSSL {
	
	private static final File CLIENT_JKS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/client_20190306.jks");
	private static final File CLIENT_TS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/client_20190306.ts");

	public static void main(String[] args)
			throws UnrecoverableKeyException, KeyManagementException, FileNotFoundException, NoSuchAlgorithmException,
			CertificateException, KeyStoreException, IOException, NotBoundException {
		SslClientSocketFactory csf = new SslClientSocketFactory(
				SSLFile.builder()
					.file(CLIENT_JKS_FILE)
					.password("pa55w0rd")
				.build(),
				
				SSLFile.builder()
					.file(CLIENT_TS_FILE)
					.password("pa55w0rd")
				.build()
			);
		Registry registry = LocateRegistry.getRegistry("localhost", 1099, csf);

		UserInfo user = (UserInfo) registry.lookup("UserInfo");
		user.addInfo(PersonalInfo.builder()
						.id(1)
						.firstName("Paulo")
						.middleName("De Los Reyes")
						.lastName("Marquez")
					.build());
		user.addInfo(PersonalInfo.builder()
						.id(2)
						.firstName("Houwaey")
						.middleName("Haewoje")
						.lastName("Touiyake")
					.build());
		
		System.out.println("ID[2]: " + user.getInfo(2));
		System.out.println("ALL: " + user.getAllInfo());
		
	}

}