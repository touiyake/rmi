package com.codinglabs.rmi.client.util;

import java.io.File;
import java.io.Serializable;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Util implements Serializable {

	public String getWorkingDirectory() {
		return (new File(System.getProperty("java.class.path").split(":")[0])).getAbsoluteFile().getParentFile()
				.getPath();
	}

}