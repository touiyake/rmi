package com.codinglabs.rmi.server.ssl;

import java.io.File;
import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class SSLFile implements Serializable {

	private String path;
	private File file;
	private String password;

}