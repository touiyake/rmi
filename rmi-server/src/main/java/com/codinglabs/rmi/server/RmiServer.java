package com.codinglabs.rmi.server;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import com.codinglabs.rmi.server.impl.UserInfoImpl;

public class RmiServer {
	
	public static void main(String[] args) throws RemoteException, MalformedURLException {
		System.out.println("RMI Server started...");
		
		try {
			Registry registry = LocateRegistry.createRegistry(1099);
			System.out.println("RMI Registry created.");
			registry.rebind("UserInfo", UnicastRemoteObject.exportObject(new UserInfoImpl(), 0));
			System.out.println("PeerServer bound in registry.");
		} catch (RemoteException e) {
			System.out.println("RMI Registry already exists.");
		}
	}

}