package com.codinglabs.rmi.server.ssl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.rmi.server.RMIClientSocketFactory;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class SslClientSocketFactory implements RMIClientSocketFactory, Serializable {
	private static final long serialVersionUID = 1L;
	private SSLSocketFactory sf = null;
	
	public SslClientSocketFactory(SSLFile jks, SSLFile ts)
			throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException,
			FileNotFoundException, IOException, KeyManagementException {
		KeyStore ks = KeyStore.getInstance("jks");
		ks.load(new FileInputStream(jks.getFile()), jks.getPassword().toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, jks.getPassword().toCharArray());
		
		KeyStore kts = KeyStore.getInstance("jks");
		kts.load(new FileInputStream(ts.getFile()), ts.getPassword().toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(kts);
		
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
		sf = sslContext.getSocketFactory();
	}

	public SslClientSocketFactory(String filename, String password)
			throws FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException,
			KeyStoreException, UnrecoverableKeyException, KeyManagementException {
		
		KeyStore ks = KeyStore.getInstance("jks");
		ks.load(new FileInputStream(new File(filename + ".jks")), password.toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, password.toCharArray());
		
		KeyStore ts = KeyStore.getInstance("jks");
		ts.load(new FileInputStream(new File(filename + ".ts")), password.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(ts);
		
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
		sf = sslContext.getSocketFactory();
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException {
		return (SSLSocket) sf.createSocket(host, port);
	}

}
