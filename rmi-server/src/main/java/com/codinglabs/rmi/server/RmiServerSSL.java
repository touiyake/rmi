package com.codinglabs.rmi.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import com.codinglabs.rmi.server.impl.UserInfoImpl;
import com.codinglabs.rmi.server.ssl.SSLFile;
import com.codinglabs.rmi.server.ssl.SslClientSocketFactory;
import com.codinglabs.rmi.server.ssl.SslServerSocketFactory;
import com.library.common.Util;

public class RmiServerSSL {
	
	private static final File SERVER_JKS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/server_20190306.jks");
	private static final File SERVER_TS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/server_20190306.ts");
	private static final File CLIENT_JKS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/client_20190306.jks");
	private static final File CLIENT_TS_FILE = new File(Util.getWorkingDirectory() + "/ext/cert/client_20190306.ts");

	public static void main(String[] args) throws UnrecoverableKeyException, KeyManagementException,
			FileNotFoundException, NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException {
		System.setProperty("java.security.policy", Util.getWorkingDirectory() + "/ext/cert/all.policy");
        System.setSecurityManager(new SecurityManager());
		System.out.println("RMI Server started...");
		
		SslServerSocketFactory ssf = new SslServerSocketFactory(
				SSLFile.builder()
					.file(SERVER_JKS_FILE)
					.password("pa55w0rd")
				.build(),
				
				SSLFile.builder()
					.file(SERVER_TS_FILE)
					.password("pa55w0rd")
				.build()
			);
		SslClientSocketFactory csf = new SslClientSocketFactory(
				SSLFile.builder()
					.file(CLIENT_JKS_FILE)
					.password("pa55w0rd")
				.build(),
				
				SSLFile.builder()
					.file(CLIENT_TS_FILE)
					.password("pa55w0rd")
				.build()
			);
		Registry registry = LocateRegistry.createRegistry(1099, csf, ssf);
		System.out.println("RMI Registry created.");
		
		registry.rebind("UserInfo", UnicastRemoteObject.exportObject(new UserInfoImpl(), 0));
		System.out.println("PeerServer bound in registry.");
	}

}