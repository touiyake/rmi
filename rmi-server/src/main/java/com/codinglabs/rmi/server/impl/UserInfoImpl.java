package com.codinglabs.rmi.server.impl;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import com.codinglabs.rmi.pub.dto.PersonalInfo;
import com.codinglabs.rmi.pub.interfaces.UserInfo;

public class UserInfoImpl implements UserInfo {
	private Map<Integer, PersonalInfo> infos = new HashMap<Integer, PersonalInfo>();

	@Override
	public void addInfo(PersonalInfo info) throws RemoteException {
		infos.put(info.getId(), info);
	}

	@Override
	public Map<Integer, PersonalInfo> getAllInfo() throws RemoteException {
		return infos;
	}

	@Override
	public PersonalInfo getInfo(int id) throws RemoteException {
		if (!infos.containsKey(id)) {
			System.out.println("Key not found ~ ID: " + id);
			return null;
		}
		return infos.get(id);
	}

}